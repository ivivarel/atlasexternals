# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  CHARDET_BINARY_PATH
#  CHARDET_PYTHON_PATH
#
# Can be steered by CHARDET_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If it was already found, let's be quiet:
if( CHARDET_FOUND )
   set( chardet_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( CHARDET_LCGROOT )
   set( _extraChardetArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()
lcg_system_ignore_path_setup()

# Find the binary path:
find_path( CHARDET_BINARY_PATH chardetect
   PATH_SUFFIXES bin PATHS ${CHARDET_LCGROOT}
   ${_extraChardetArgs} )

# Find the python path:
find_path( CHARDET_PYTHON_PATH
   NAMES chardet/__init__.py
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${CHARDET_LCGROOT}
   ${_extraChardetArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( chardet DEFAULT_MSG
   CHARDET_PYTHON_PATH CHARDET_BINARY_PATH )

# Set up the RPM dependency:
lcg_need_rpm( chardet )

# Clean up:
if( _extraChardetArgs )
   unset( _extraChardetArgs )
endif()
