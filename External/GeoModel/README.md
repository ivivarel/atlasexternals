GeoModel Kernel Library
=========================

This package builds the GeoModel Kernel library for the offline software of ATLAS.

The library's sources are taken from https://gitlab.cern.ch/GeoModelDev/GeoModelKernel
